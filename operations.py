# -- coding: utf-8 --

"""
Created on Fri Mar 13 15:10:17 2020

@author: edouard.roger
"""

class SimpleCalculator:

    """ This class allows us to perform various operations """

    def __init__(self, var_a, var_b):
        self.var_a = var_a
        self.var_b = var_b
        self.result = 0

    def sum(self):

        """ Objective: Do a sum between 2 values

    Parameters:
        var_a: First variables for sum
        var_b: Second variables for sum

    Returns:
        result: The sum's result
        """

        if isinstance(self.var_a, int) and isinstance(self.var_b, int):
            result = self.var_a + self.var_b
            return result
        return "ERROR"

    def subtract(self):

        """ Objective: Do a substraction between 2 values

    Parameters:
        var_a: First variables for substraction
        var_b: Second variables for substraction

    Returns:
        result: The dubstraction's result
        """

        if isinstance(self.var_a, int) and isinstance(self.var_b, int):
            result = self.var_a - self.var_b
            return result
        return "ERROR"

    def multiply(self):

        """ Objective: Do a multiplication between 2 values

    Parameters:
        var_a: First variables for multiplication
        var_b: Second variables for multiplication

    Returns:
        result: The multiplication's result
        """

        if isinstance(self.var_a, int) and isinstance(self.var_b, int):
            result = self.var_a * self.var_b
            return result
        return "ERROR"

    def divide(self):

        """ Objective: Do a division between 2 values

    Parameters:
        var_a: First variables for division
        var_b: Second variables for division

    Returns:
        result: The division's result
        """

        if isinstance(self.var_a, int) and isinstance(self.var_b, int):
            result = self.var_a / self.var_b
            return result
        if self.var_b == 0:
            raise ZeroDivisionError("Cannot divide by zero")
        return "ERROR"

